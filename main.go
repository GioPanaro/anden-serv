/*
Copyright (c) 2018 Giovanni Panaro
*/

package main

import (
	"net/http"
	"fmt"
	"log"
	"html/template"
	"os"
	"io"
	"time"
)

const MEGABYTE = 1 << 20

type AndenMux struct{}

func test(w http.ResponseWriter) {
	fmt.Fprintf(w, "Hey!")
}

func stolenCookies(w http.ResponseWriter, r *http.Request) {
	stolen, err := r.Cookie("stolen")
	if err != nil {
		set := false
		if r.Method == "POST" {
			r.ParseForm()
			expiration := time.Now().Add(time.Hour)
			cookie := http.Cookie{
				Name: "stolen",
				Value: r.FormValue("stolen"),
				Expires: expiration,
			}
			http.SetCookie(w, &cookie)
			stolen, _ = r.Cookie("stolen")
			set = true
		}
		if !set {
			token := generateToken()
			t, _ := template.ParseFiles("./dynamic/html/cookies-unset.html")
			t.Execute(w, token)
		}
		return
	}
	if stolen.Value == "true" {
		fmt.Fprintf(w, "Why you steal it homie?")
	} else {
		fmt.Fprintf(w, "I dont trust ya")
	}
}

func upload(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		token := generateToken()
		t, _ := template.ParseFiles("./dynamic/html/upload.html")
		t.Execute(w, token)
		return
	}
	r.ParseMultipartForm(MEGABYTE * 20) //Max upload size of 20 MB
	file, handler, err := r.FormFile("file")
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()
	fmt.Fprint(w, handler.Header)
	f, err := os.OpenFile("./static/uploads/" + handler.Filename,
		os.O_WRONLY | os.O_CREATE, 0666)
	if err != nil {
		log.Println(err)
		return
	}
	defer f.Close()
	io.Copy(f, file)
}

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		token := generateToken()
		t, _ := template.ParseFiles("./dynamic/html/login.html")
		t.Execute(w, token)
		return
	}
	r.ParseForm()
	token := r.FormValue("token")
	if token == "" {
		log.Println("Error: no token sent")
	}
	fmt.Println("Username: ", r.FormValue("username"))
	fmt.Println("Password: ", r.FormValue("password"))
}

func (p *AndenMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		test(w)
	case "/login":
		login(w, r)
	case "/upload":
		upload(w, r)
	case "/cookies":
		stolenCookies(w, r)
	default:
		http.NotFound(w, r)
	}
}

func main() {
	mux := &AndenMux{}
	log.Fatal(http.ListenAndServe(":8080", mux))
}
