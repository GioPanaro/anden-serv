package main

import "sync"

type Manager struct {
	cookieName string
	lock sync.Mutex
	provider Provider
	maxLifeTime uint64
}

func newManager