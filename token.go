package main

import (
	"time"
	"crypto/md5"
	"io"
	"strconv"
	"fmt"
)

func generateToken() string {
	currentTime := time.Now().Unix()
	h := md5.New()
	io.WriteString(h, strconv.FormatInt(currentTime, 10))
	token := fmt.Sprintf("%x", h.Sum(nil))
	return token
}